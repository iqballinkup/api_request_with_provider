// To parse this JSON data, do
//
//     final allProductsModel = allProductsModelFromJson(jsonString);

import 'dart:convert';

AllProductsModel allProductsModelFromJson(String str) =>
    AllProductsModel.fromJson(json.decode(str));

String allProductsModelToJson(AllProductsModel data) =>
    json.encode(data.toJson());

class AllProductsModel {
  AllProductsModel({
    this.data,
  });

  final List<Datum>? data;

  factory AllProductsModel.fromJson(Map<String, dynamic> json) =>
      AllProductsModel(
        data: json["data"] == null
            ? []
            : List<Datum>.from(json["data"]!.map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.productCode,
    this.name,
    this.slug,
    this.model,
    required this.price,
    this.simillarPorduct,
    this.similarDiscount,
    this.sizeId,
    this.colorId,
    this.categoryId,
    this.subCategoryId,
    this.brandId,
    this.discount,
    this.shortDetails,
    this.description,
    this.mainImage,
    this.thumbImage,
    this.smallImage,
    this.sizeguide,
    this.isFeature,
    this.isCollectionTitle1,
    this.isCollectionTitle2,
    this.isDeal,
    this.isTailoring,
    this.tailoringCharge,
    this.isTrending,
    this.newArrival,
    this.dealStart,
    this.dealEnd,
    this.rewardPoint,
    this.status,
    required this.quantity,
    this.saveBy,
    this.updateBy,
    this.ipAddress,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
    this.sizes,
    this.colors,
    this.currencyAmount,
    this.category,
  });

  final int? id;
  final String? productCode;
  final String? name;
  final String? slug;
  final String? model;
  final int price;
  final String? simillarPorduct;
  final String? similarDiscount;
  final String? sizeId;
  final String? colorId;
  final int? categoryId;
  final int? subCategoryId;
  final int? brandId;
  final String? discount;
  final String? shortDetails;
  final String? description;
  final String? mainImage;
  final String? thumbImage;
  final String? smallImage;
  final String? sizeguide;
  final String? isFeature;
  final String? isCollectionTitle1;
  final String? isCollectionTitle2;
  final String? isDeal;
  final String? isTailoring;
  final dynamic tailoringCharge;
  final String? isTrending;
  final String? newArrival;
  final dynamic dealStart;
  final dynamic dealEnd;
  final String? rewardPoint;
  final int? status;
  final int quantity;
  final String? saveBy;
  final dynamic updateBy;
  final IpAddress? ipAddress;
  final dynamic deletedAt;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  final List<Color>? sizes;
  final List<Color>? colors;
  final String? currencyAmount;
  final Category? category;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        productCode: json["product_code"],
        name: json["name"],
        slug: json["slug"],
        model: json["model"],
        price: json["price"],
        simillarPorduct: json["simillar_porduct"],
        similarDiscount: json["similar_discount"],
        sizeId: json["size_id"],
        colorId: json["color_id"],
        categoryId: json["category_id"],
        subCategoryId: json["sub_category_id"],
        brandId: json["brand_id"],
        discount: json["discount"],
        shortDetails: json["short_details"],
        description: json["description"],
        mainImage: json["main_image"],
        thumbImage: json["thumb_image"],
        smallImage: json["small_image"],
        sizeguide: json["sizeguide"],
        isFeature: json["is_feature"],
        isCollectionTitle1: json["is_collection_title_1"],
        isCollectionTitle2: json["is_collection_title_2"],
        isDeal: json["is_deal"],
        isTailoring: json["is_tailoring"],
        tailoringCharge: json["tailoring_charge"],
        isTrending: json["is_trending"],
        newArrival: json["new_arrival"],
        dealStart: json["deal_start"],
        dealEnd: json["deal_end"],
        rewardPoint: json["reward_point"],
        status: json["status"],
        quantity: json["quantity"],
        saveBy: json["save_by"],
        updateBy: json["update_by"],
        ipAddress: ipAddressValues.map[json["ip_address"]]!,
        deletedAt: json["deleted_at"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        sizes: json["sizes"] == null
            ? []
            : List<Color>.from(json["sizes"]!.map((x) => Color.fromJson(x))),
        colors: json["colors"] == null
            ? []
            : List<Color>.from(json["colors"]!.map((x) => Color.fromJson(x))),
        currencyAmount: json["currency_amount"],
        category: json["category"] == null
            ? null
            : Category.fromJson(json["category"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "product_code": productCode,
        "name": name,
        "slug": slug,
        "model": model,
        "price": price,
        "simillar_porduct": simillarPorduct,
        "similar_discount": similarDiscount,
        "size_id": sizeId,
        "color_id": colorId,
        "category_id": categoryId,
        "sub_category_id": subCategoryId,
        "brand_id": brandId,
        "discount": discount,
        "short_details": shortDetails,
        "description": description,
        "main_image": mainImage,
        "thumb_image": thumbImage,
        "small_image": smallImage,
        "sizeguide": sizeguide,
        "is_feature": isFeature,
        "is_collection_title_1": isCollectionTitle1,
        "is_collection_title_2": isCollectionTitle2,
        "is_deal": isDeal,
        "is_tailoring": isTailoring,
        "tailoring_charge": tailoringCharge,
        "is_trending": isTrending,
        "new_arrival": newArrival,
        "deal_start": dealStart,
        "deal_end": dealEnd,
        "reward_point": rewardPoint,
        "status": status,
        "quantity": quantity,
        "save_by": saveBy,
        "update_by": updateBy,
        "ip_address": ipAddressValues.reverse[ipAddress],
        "deleted_at": deletedAt,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "sizes": sizes == null
            ? []
            : List<dynamic>.from(sizes!.map((x) => x.toJson())),
        "colors": colors == null
            ? []
            : List<dynamic>.from(colors!.map((x) => x.toJson())),
        "currency_amount": currencyAmount,
        "category": category?.toJson(),
      };
}

class Category {
  Category({
    this.id,
    this.name,
    this.slug,
    this.details,
    this.image,
    this.thumbimage,
    this.smallimage,
    this.status,
    this.isHomepage,
    this.isMenu,
    this.saveBy,
    this.updatedBy,
    this.ipAddress,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
  });

  final int? id;
  final String? name;
  final String? slug;
  final String? details;
  final String? image;
  final String? thumbimage;
  final String? smallimage;
  final Status? status;
  final String? isHomepage;
  final String? isMenu;
  final String? saveBy;
  final dynamic updatedBy;
  final IpAddress? ipAddress;
  final dynamic deletedAt;
  final DateTime? createdAt;
  final DateTime? updatedAt;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
        name: json["name"],
        slug: json["slug"],
        details: json["details"],
        image: json["image"],
        thumbimage: json["thumbimage"],
        smallimage: json["smallimage"],
        status: statusValues.map[json["status"]]!,
        isHomepage: json["is_homepage"],
        isMenu: json["is_menu"],
        saveBy: json["save_by"],
        updatedBy: json["updated_by"],
        ipAddress: ipAddressValues.map[json["ip_address"]]!,
        deletedAt: json["deleted_at"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "slug": slug,
        "details": details,
        "image": image,
        "thumbimage": thumbimage,
        "smallimage": smallimage,
        "status": statusValues.reverse[status],
        "is_homepage": isHomepage,
        "is_menu": isMenu,
        "save_by": saveBy,
        "updated_by": updatedBy,
        "ip_address": ipAddressValues.reverse[ipAddress],
        "deleted_at": deletedAt,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
      };
}

enum IpAddress { THE_1031597331, THE_127001, THE_10315415843 }

final ipAddressValues = EnumValues({
  "103.154.158.43": IpAddress.THE_10315415843,
  "103.159.73.31": IpAddress.THE_1031597331,
  "127.0.0.1": IpAddress.THE_127001
});

enum Status { A }

final statusValues = EnumValues({"a": Status.A});

class Color {
  Color({
    this.id,
    this.name,
    this.code,
    this.createdAt,
    this.updatedAt,
  });

  final int? id;
  final Name? name;
  final Code? code;
  final DateTime? createdAt;
  final DateTime? updatedAt;

  factory Color.fromJson(Map<String, dynamic> json) => Color(
        id: json["id"],
        name: nameValues.map[json["name"]],
        code: codeValues.map[json["code"]],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": nameValues.reverse[name],
        "code": codeValues.reverse[code],
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
      };
}

enum Code { THE_040101, FD0808, THE_101_FEA }

final codeValues = EnumValues({
  "#fd0808": Code.FD0808,
  "#040101": Code.THE_040101,
  "#101fea": Code.THE_101_FEA
});

enum Name { BLACK, RED, BLUE, S, M, XL, L, THE_2_XL }

final nameValues = EnumValues({
  "Black": Name.BLACK,
  "Blue": Name.BLUE,
  "L": Name.L,
  "M": Name.M,
  "red": Name.RED,
  "S": Name.S,
  "2XL": Name.THE_2_XL,
  "XL": Name.XL
});

class EnumValues<T> {
  Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap = map.map((k, v) => MapEntry(v, k));
    return reverseMap;
  }
}
