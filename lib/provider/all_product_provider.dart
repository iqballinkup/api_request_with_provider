import 'package:api_request_with_provider/api/api_class.dart';
import 'package:api_request_with_provider/model/all_product_model.dart';
import 'package:flutter/material.dart';

import '../model/hot_deal_model.dart';

class AllProductProvider extends ChangeNotifier {
  List<Datum> provideAllProductList = [];
  getAllProduct(context) async {
    provideAllProductList = await ApiClass.fetchAllProducts(context);
    notifyListeners();
  }
}

class HotDealProvider extends ChangeNotifier {
  List<Datam> provideHotDealProductList = [];
  getHotDealProduct(context) async {
    provideHotDealProductList = await ApiClass.fetchHotDealProducts(context);
    notifyListeners();
  }
}
