import 'dart:convert';
import 'package:http/http.dart' as http;

import '../model/all_product_model.dart';
import '../model/hot_deal_model.dart';

class ApiClass {
//All Products Request
  static Future<List<Datum>> fetchAllProducts(context) async {
    List<Datum> allProductList = [];
    try {
      final response = await http.get(Uri.parse("https://bornonbd.com/api/product"));
      Datum datum;
      if (response.statusCode == 200) {
        var data = jsonDecode(response.body);
        print("==========all products==========$data");
        for (var i in data["data"]) {
          datum = Datum.fromJson(i);
          allProductList.add(datum);
        }
      } else {
        throw Exception('Failed to load hotDealProductsList');
      }
    } catch (e) {
      Future.error("=====Errrrrrrrooooorrr=====$e");
    }
    return allProductList;
  }

//HotDeal Products Request
  static Future<List<Datam>> fetchHotDealProducts(context) async {
    List<Datam> hotDealProductsList = [];
    try {
      final response = await http.get(Uri.parse("https://bornonbd.com/api/hot-deal-product"));
      Datam datam;
      if (response.statusCode == 200) {
        var data = jsonDecode(response.body);
        print("length========${data["data"]["data"].length}");
        for (var i in data["data"]["data"]) {
          datam = Datam.fromJson(i);
          hotDealProductsList.add(datam);
        }
      } else {
        throw Exception('Failed to load hotDealProductsList');
      }
    } catch (e) {
      Future.error("=====Errrrrrrrooooorrr=====$e");
    }
    return hotDealProductsList;
  }
}
