import 'package:api_request_with_provider/provider/all_product_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../constant/api_const.dart';

class AllProductPage extends StatefulWidget {
  const AllProductPage({Key? key}) : super(key: key);

  @override
  State<AllProductPage> createState() => _AllProductPageState();
}

class _AllProductPageState extends State<AllProductPage> {
  @override
  void initState() {
    // TODO: implement initState
    Provider.of<AllProductProvider>(context, listen: false).getAllProduct(context);
    super.initState();
  }

  double productTextSize = 14.0;
  double productPriceSize = 14.0;

  @override
  Widget build(BuildContext context) {
    final allProductList = Provider.of<AllProductProvider>(context).provideAllProductList;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: CustomScrollView(
          slivers: [
            SliverPadding(
              padding: EdgeInsets.only(
                top: 10,
                bottom: 0,
                left: 20,
                right: 20,
              ),
              sliver: SliverGrid(
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 300,
                  mainAxisSpacing: 16,
                  crossAxisSpacing: MediaQuery.of(context).size.width * 0.04,
                  mainAxisExtent: 280,
                ),
                delegate: SliverChildBuilderDelegate((context, index) {
                  return Card(
                      elevation: 8.0,
                      shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(topRight: Radius.circular(20), topLeft: Radius.circular(20))),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 10.0),
                            height: 200,
                            child: Image.network(
                              "$mainImageURL${allProductList[index].mainImage}",
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.only(left: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${allProductList[index].name}",
                                  style: TextStyle(fontSize: productTextSize),
                                ),
                                Text(
                                  r'$' "${allProductList[index].price}",
                                  style: TextStyle(
                                      fontSize: productTextSize, color: Colors.blue, fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          )
                        ],
                      ));
                }, childCount: allProductList.length),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
