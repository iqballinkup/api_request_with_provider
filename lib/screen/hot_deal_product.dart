import 'package:api_request_with_provider/provider/all_product_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../constant/api_const.dart';

class HotDealProductPage extends StatefulWidget {
  const HotDealProductPage({Key? key}) : super(key: key);

  @override
  State<HotDealProductPage> createState() => _HotDealProductPageState();
}

class _HotDealProductPageState extends State<HotDealProductPage> {
  @override
  void initState() {
    // TODO: implement initState
    Provider.of<HotDealProvider>(context, listen: false).getHotDealProduct(context);
    super.initState();
  }

  double productTextSize = 14.0;
  double productPriceSize = 14.0;

  @override
  Widget build(BuildContext context) {
    final hotDealProductList = Provider.of<HotDealProvider>(context).provideHotDealProductList;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: CustomScrollView(
          slivers: [
            SliverPadding(
              padding: EdgeInsets.only(
                top: 10,
                bottom: 0,
                left: 20,
                right: 20,
              ),
              sliver: SliverGrid(
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 300,
                  mainAxisSpacing: 16,
                  crossAxisSpacing: MediaQuery.of(context).size.width * 0.04,
                  mainAxisExtent: 280,
                ),
                delegate: SliverChildBuilderDelegate((context, index) {
                  return Card(
                      elevation: 8.0,
                      shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(topRight: Radius.circular(20), topLeft: Radius.circular(20))),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 10.0),
                            height: 200,
                            child: Image.network(
                              "$mainImageURL${hotDealProductList[index].mainImage}",
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.only(left: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${hotDealProductList[index].name}",
                                  style: TextStyle(fontSize: productTextSize),
                                ),
                                Text(
                                  r'$' "${hotDealProductList[index].price}",
                                  style: TextStyle(
                                      fontSize: productTextSize, color: Colors.blue, fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          )
                        ],
                      ));
                }, childCount: hotDealProductList.length),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
